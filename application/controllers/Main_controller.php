<?php 

class Main_controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
    }

    function index() {
        $data = array();

        $data["listing"] = $this->Main_model->getData();

        $this->load->view("Main_view",$data);
    }

    function fetchPrice() {
        $price = $_GET['priceValue'];

        if($price == 1000000) {
            $maxPrice = 1000000;
            $minPrice = 0;

        } else if ($price == 2000000) {
            $maxPrice = 2000000;
            $minPrice = 1000001;
        } else {
            $maxPrice = 20000000;
            $minPrice = 2000001;
        }

        $data['fetchprice'] = $this->Main_model->Price($maxPrice, $minPrice);

        print_r(json_encode($data));
    }

    function fetchModel() {
        $modelnumber = $_GET['model'];

        $data['fetchmodel'] = $this->Main_model->ModelNumber($modelnumber);

        print_r(json_encode($data));
    }

    function fetchGear() {
        $gear = $_GET['gear'];

        $data['fetchgear'] = $this->Main_model->Gear($gear);

        print_r(json_encode($data));
    }

    function fetchNameColor() {
        $value = $_GET['value'];

        $data['namecolor'] = $this->Main_model->NAMECOLOR($value);

        print_r(json_encode($data));
    }
}

?>