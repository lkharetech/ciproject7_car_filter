<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        table,
        th,
        td {
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
        }

        td,
        b {
            width: 17%;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <title>CAR PROJECT</title>
</head>

<body>
    <div class="container">
        <hr>
        <h1>CAR SEARCH & FILTER PROJECT</h1>
        <hr>
        <div>
            <div>
                <input type="search" id="search" name="search"> Search by Car name or Color
            </div><br>
            <div>
                <select id="gear" name="gear">
                    <option value="">--Select Gear Type--</option>
                    <option value="vxl">vxl</option>
                    <option value="xxl">xxl</option>
                    <option value="hvv">hvv</option>
                    <option value="ytt">ytt</option>
                    <option value="klkk">klkk</option>
                </select>
            </div><br>
            <div>
                <select id="modelnumber" name="modelnumber">
                    <option value="">--Select Model Number--</option>
                    <?php $selected = "";
                    foreach ($listing as $row) { ?>
                        <option value="<?php echo $row['modelnumber'] ?>" <?php echo $selected; ?>>
                            <?php echo $row['modelnumber']; ?></option>
                    <?php } ?>
                </select>
            </div>
            <br>
            <div>
                <select id="price" name="price">
                    <option value="">--Select Price--</option>
                    <option value="1000000">Below 10 Lakh</option>
                    <option value="2000000">Between 10 Lakh to 20 Lakh</option>
                    <option value="2100000">Above 20 Lakh</option>
                </select>
            </div>
        </div>
        <hr>
        <div>
            <table style="text-align: center;">
                <thead>
                    <tr>
                        <td><b>#</b></td>
                        <td><b>Car Name<b></td>
                        <td><b>Color<b></td>
                        <td><b>Model No.<b></td>
                        <td><b>Gear<b></td>
                        <td><b>Price<b></td>
                    </tr>
                </thead>
                <tbody id="tableData">
                    <?php $count = 0;
                    foreach ($listing as $row) { ?>
                        <tr>
                            <td><?php echo ++$count;  ?></td>
                            <td><?php echo $row['carname']; ?></td>
                            <td><?php echo $row['color']; ?></td>
                            <td><?php echo $row['modelnumber']; ?></td>
                            <td><?php echo $row['gear']; ?></td>
                            <td><?php echo $row['price']; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <hr>
        </div>
    </div>

</body>

</html>

<script>
    $(document).ready(function() {

        $("#price").change(function() {
            var priceValue = $('option:selected', this).val();

            $.ajax({
                url: "<?php echo base_url(); ?>fetchPrice",
                type: 'get',
                data: {
                    priceValue: priceValue,
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    console.log(response.fetchprice);

                    var tr = "";
                    let count = 0;
                    let row = "";
                    for (var i = 0; i < response.fetchprice.length; i++) {
                        var carname = response.fetchprice[i].carname;
                        var color = response.fetchprice[i].color;
                        var modelnumber = response.fetchprice[i].modelnumber;
                        var gear = response.fetchprice[i].gear;
                        var price = response.fetchprice[i].price;


                        tr += '<tr>';
                        tr += '<td>' + ++count + '</td>';
                        tr += '<td>' + carname + '</td>';
                        tr += '<td>' + color + '</td>';
                        tr += '<td>' + modelnumber + '</td>';
                        tr += '<td>' + gear + '</td>';
                        tr += '<td>' + price + '</td>';
                        tr += '</tr>';
                    }
                    $('#tableData').html(tr);
                }
            });
        });

        $("#modelnumber").change(function() {
            var model = $('option:selected', this).val();

            $.ajax({
                url: "<?php echo base_url(); ?>fetchModel",
                type: 'get',
                data: {
                    model: model,
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    console.log(response.fetchmodel);

                    var tr = "";
                    let count = 0;
                    let row = "";
                    for (var i = 0; i < response.fetchmodel.length; i++) {
                        var carname = response.fetchmodel[i].carname;
                        var color = response.fetchmodel[i].color;
                        var modelnumber = response.fetchmodel[i].modelnumber;
                        var gear = response.fetchmodel[i].gear;
                        var price = response.fetchmodel[i].price;


                        tr += '<tr>';
                        tr += '<td>' + ++count + '</td>';
                        tr += '<td>' + carname + '</td>';
                        tr += '<td>' + color + '</td>';
                        tr += '<td>' + modelnumber + '</td>';
                        tr += '<td>' + gear + '</td>';
                        tr += '<td>' + price + '</td>';
                        tr += '</tr>';
                    }
                    $('#tableData').html(tr);
                }
            })
        });

        $("#gear").change(function() {
            var gear = $('option:selected', this).val();

            $.ajax({
                url: "<?php echo base_url(); ?>fetchGear",
                type: 'get',
                data: {
                    gear: gear,
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    console.log(response.fetchgear);

                    var tr = "";
                    let count = 0;
                    let row = "";
                    for (var i = 0; i < response.fetchgear.length; i++) {
                        var carname = response.fetchgear[i].carname;
                        var color = response.fetchgear[i].color;
                        var modelnumber = response.fetchgear[i].modelnumber;
                        var gear = response.fetchgear[i].gear;
                        var price = response.fetchgear[i].price;


                        tr += '<tr>';
                        tr += '<td>' + ++count + '</td>';
                        tr += '<td>' + carname + '</td>';
                        tr += '<td>' + color + '</td>';
                        tr += '<td>' + modelnumber + '</td>';
                        tr += '<td>' + gear + '</td>';
                        tr += '<td>' + price + '</td>';
                        tr += '</tr>';
                    }
                    $('#tableData').html(tr);
                }
            })
        });

        $("#search").keyup(function() {
            var value = $(this).val();

            // alert(value);
            $.ajax({
                url: "<?php echo base_url(); ?>fetchNameColor",
                type: 'get',
                data: {
                    value: value,
                },
                success: function(data) {
                    var response = JSON.parse(data);
                    console.log(response.namecolor);

                    var tr = "";
                    let count = 0;
                    let row = "";
                    for (var i = 0; i < response.namecolor.length; i++) {
                        var carname = response.namecolor[i].carname;
                        var color = response.namecolor[i].color;
                        var modelnumber = response.namecolor[i].modelnumber;
                        var gear = response.namecolor[i].gear;
                        var price = response.namecolor[i].price;


                        tr += '<tr>';
                        tr += '<td>' + ++count + '</td>';
                        tr += '<td>' + carname + '</td>';
                        tr += '<td>' + color + '</td>';
                        tr += '<td>' + modelnumber + '</td>';
                        tr += '<td>' + gear + '</td>';
                        tr += '<td>' + price + '</td>';
                        tr += '</tr>';
                    }
                    $('#tableData').html(tr);
                }
            });
        });
    });
</script>