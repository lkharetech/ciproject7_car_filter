<?php

class Main_model extends CI_Model
{

    function getData()
    {
        $query = $this->db->get('users');
        return $query->result_array();
    }

    function Price($maxPrice, $minPrice)
    {
        $this->db->where('price >=', $minPrice);
        $this->db->where('price <=', $maxPrice);
        $query = $this->db->get('users');
        return $query->result_array();
    }

    function ModelNumber($modelnumber)
    {
        $this->db->where('modelnumber', $modelnumber);
        $query = $this->db->get('users');
        return $query->result_array();
    }

    function Gear($gear)
    {
        $this->db->where('gear', $gear);
        $query = $this->db->get('users');
        return $query->result_array();
    }

    function NAMECOLOR($value)
    {
        $this->db->like('carname', $value);
        $this->db->or_like('color', $value);
        $query = $this->db->get('users');
        return $query->result_array();
    }
}
